module gitlab.com/SparrowOchon/site-mapper

go 1.17

require (
	github.com/akamensky/argparse v1.3.1
	github.com/anaskhan96/soup v1.2.5
	github.com/fatih/color v1.13.0
	github.com/panjf2000/ants v1.3.0
)

require (
	github.com/mattn/go-colorable v0.1.9 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	golang.org/x/net v0.0.0-20220401154927-543a649e0bdd // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
)
