package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"runtime"

	"github.com/akamensky/argparse"
	"github.com/fatih/color"
	"gitlab.com/SparrowOchon/site-mapper/src/lib/utility"

	"log"
	"os"
)

type UserInput struct {
	depth       *int
	output_path *string
	url         *string
}

/**
 * @Description: Validate user passed in parameters
 * @param info Input buffer from CLI
 * @return *UserInput Struct of user passed data
 */
func validate_input_parameters() *UserInput {
	const DEFAULT_MAX_DEPTH = 10

	cli_parser := argparse.NewParser("site-mapper",
		"Create a sitemap given a URL."+
			"\n Examples: "+
			"     site-mapper -u http://google.com -m 10\n")
	var max_depth = cli_parser.Int("m", "max-depth",
		&argparse.Options{Required: false,
			Help: "Max Depth.(Default: 10)"})
	var output_location = cli_parser.String("o", "output",
		&argparse.Options{Required: false,
			Help: "Output Location (Defaults to PWD/output)"})
	var start_url = cli_parser.String("u", "url",
		&argparse.Options{Required: true,
			Help: "Url to map"})
	err := cli_parser.Parse(os.Args)
	if err != nil {
		log.Fatal(cli_parser.Usage(err))
	}
	if !is_valid_url(*start_url) {
		os.Exit(1)
	}
	// if the dir doesnt exist or not provided we make a temp file.
	if !is_valid_path(*output_location) {
		*output_location = make_tmp_file()
		fmt.Println("Invalid output path. Output will be stored at" +
			*output_location)
	}
	if *max_depth <= 0 {
		*max_depth = DEFAULT_MAX_DEPTH // Make sure its always Positive
	}
	return &UserInput{depth: max_depth,
		output_path: output_location, url: start_url}
}

/**
 * @Description: Identify if file exists
 * @param path of file to validate
 * @return bool status of file existence
 */
func is_valid_path(path string) bool {
	info, err := os.Stat(path)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

/**
 * @Description: Identify if URL is returning a 200 or 300 status code
 * @param url whose status we want to check
 * @return bool status of the website status code in range
 */
func is_valid_url(url string) bool {
	client := http.Client{
		Timeout: utility.URL_CONNECTION_TIMEOUT,
	}
	resp, err := client.Get(url)
	if err != nil {
		color.Red("Error: Unable to connect to Url")
		return false
	}
	defer resp.Body.Close()
	if resp.StatusCode > 200 && resp.StatusCode < 399 {
		color.Red("Error: URL returns non 200 or 300 status code")
		return false
	}
	return true
}

/**
 * @Description: Make a new txt temp file. In the PWD under a sub-directory /output/
 * @return string  The full path to the new file made
 */
func make_tmp_file() string {
	var output_file_dir string
	if runtime.GOOS == "windows" {
		output_file_dir = "\\output"
	} else {
		output_file_dir = "/output"
	}
	dir_name, err := os.Getwd()
	if err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}
	dir_name += output_file_dir
	err = os.MkdirAll(dir_name, os.ModePerm)
	if err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}
	file, err := ioutil.TempFile(dir_name, "site-mapper_log.*.json")
	if err != nil {
		color.Red(err.Error())
		os.Exit(1)
	}
	return file.Name()
}
