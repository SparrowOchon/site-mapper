package utility

import (
	"bytes"
	"encoding/json"
	"net/url"
	"strings"
	"time"
)

/**
 * @Description: Map that holds the url and its associated depth from the original user provided URL
 */
type NewUrls = map[string]int

/**
 * @Description: Struct to hold URL parsed data.
 */
type ParsedPages = map[string]PageMap

/**
 * @Description: Struct to hold URL parsed data.
 */
type PageMap struct {
	CurrUrl   string   `json:"page_url"`
	Pages     []string `json:"links"`
	Images    []string `json:"images"`
	NextDepth int      `json:"-"`
}

/**
 * @Description: Turn a url into its base url.
 * @param test_url Url which we will convert to its base form.
 * @return string base url from domain
 */
func UrlHostname(test_url string) string {
	const MIN_URL_PARTS = 2 // i.e example.com

	u, err := url.Parse(test_url)
	if err != nil {
		return "-1"
	}
	parts := strings.Split(u.Hostname(), ".")
	if len(parts) > MIN_URL_PARTS {
		return parts[len(parts)-2] + "." + parts[len(parts)-1]
	} else {
		return u.Hostname()
	}
}

/**
 * @Description: Marshall to JSON without escaping HTML like is done by default
 * @param t Interface whose data we will turn to JSON
 * @return []bytes return marshaled JSON bytes array
 * @return error return error that occurred during encoding.
 */
func JSONMarshal(t interface{}) ([]byte, error) {
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)
	err := encoder.Encode(t)
	return buffer.Bytes(), err
}

/**
 * @Description: Timeout for URL to respond. Some sites will start blocking requests past a certain number.
 */
const URL_CONNECTION_TIMEOUT = 6 * time.Second
