package thread

import (
	"fmt"
	"runtime"
	"sync"

	"github.com/panjf2000/ants"
	"gitlab.com/SparrowOchon/site-mapper/src/lib/utility"
)

type ThreadData struct {
	UrlData  utility.NewUrls
	PageData chan<- utility.PageMap
}

/**
 * @Description: Initialize thread pool and launch it.
 * @param input_url Channel of urls that need to be scanned
 * @param output_pagemap Channel to out url mapped struct to
 * @return *ants.PoolWithFunc Initialized thread pool
 * @return *sync.WaitGroup Wait group for the thread pool
 */
func Make_Pool(input_url <-chan utility.NewUrls, output_pagemap chan<- utility.PageMap) (*ants.PoolWithFunc,
	*sync.WaitGroup) {

	thread_pool, pool_wait_group := build_thread_pool()
	go launch_threads(input_url, output_pagemap, thread_pool, pool_wait_group)
	return thread_pool, pool_wait_group
}

/**
 * @Description: Build a thread pool
 * @return *ants.PoolWithFunc Initialized pool creation object
 * @return *sync.WaitGroup
 */
func build_thread_pool() (*ants.PoolWithFunc, *sync.WaitGroup) {
	const CPU_MULTIPLIER = 6 // Multiplier for thread count of pool.

	thread_count := runtime.NumCPU() * CPU_MULTIPLIER
	var pool_wait_group sync.WaitGroup
	thread_pool, _ := ants.NewPoolWithFunc(thread_count, func(i interface{}) {
		Url_Parse(i)
		pool_wait_group.Done()
	})
	return thread_pool, &pool_wait_group
}

/**
 * @Description: Launch work on a thread from the pool
 * @param input_url Channel of messages read from the pipe
 * @param output_pagemap Channel of Data returned from the Thread
 * @param thread_pool Initialized thread pool
 * @param pool_wait_group Wait group of thread pool. Keep track of whats running
 */
func launch_threads(input_url <-chan utility.NewUrls, output_pagemap chan<- utility.PageMap,
	thread_pool *ants.PoolWithFunc, pool_wait_group *sync.WaitGroup) {

	/**
	Non-terminating
	**/
	defer close(output_pagemap)
	for {
		line, alive := <-input_url
		if !alive {
			break
		}
		pool_wait_group.Add(1)
		_ = thread_pool.Invoke(ThreadData{
			UrlData:  line,
			PageData: output_pagemap})
	}
	pool_wait_group.Wait()
	fmt.Println("Terminating Pool")
}
