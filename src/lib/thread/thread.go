package thread

import (
	"net/http"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/anaskhan96/soup"
	"gitlab.com/SparrowOchon/site-mapper/src/lib/utility"
)

/**
 * @Description: Fetch url data and identify all Links and Images in the url HTML
 * @param i (ThreadData) that is passed by the master thread to each thread in the pool
 */
var core_url = regexp.MustCompile(`^[^#?]+`)

func Url_Parse(i interface{}) {

	thread_data := i.(ThreadData)

	for curr_url, curr_depth := range thread_data.UrlData {
		var new_page utility.PageMap
		curr_depth++
		new_page.NextDepth = curr_depth
		new_page.CurrUrl = curr_url
		parent_url := "http://" + utility.UrlHostname(curr_url)

		client := http.Client{
			Timeout: utility.URL_CONNECTION_TIMEOUT,
		}
		resp, err := soup.GetWithClient(curr_url, &client)
		if err != nil {
			continue
		}
		doc := soup.HTMLParse(resp)

		fetch_html_links(&doc, parent_url, &new_page.Images, "src", "img")
		fetch_html_links(&doc, parent_url, &new_page.Pages, "href", "a")

		thread_data.PageData <- new_page
	}
}

/**
 * @Description: Search for links in the same domain as a user provided parent URL. Links will be looked under Tags
	and their associated url_parameters I.e a href or img src. Once a Link is found it will be loaded into a link_list
 * @param doc Channel containing the Threadpool output that we will read.
 * @param parent_url Channel containing new urls to be scanned by the Threadpool
 * @param link_list Map of collected data to be written to file.
 * @param url_parameter User provided max depth to scan to
 * @param tags User provided url.
*/
func fetch_html_links(doc *soup.Root, parent_url string, link_list *[]string, url_parameter string, tags ...string) {
	const MIN_LINK_SIZE = 2

	for _, tag := range tags {
		temp_list := doc.FindAllStrict(tag)
		var output_url string
		for _, link := range temp_list {
			if len(link.Attrs()[url_parameter]) < MIN_LINK_SIZE {
				continue
			}
			if strings.HasPrefix(link.Attrs()[url_parameter], "http") {
				output_url = link.Attrs()[url_parameter]
			} else {
				output_url = parent_url + link.Attrs()[url_parameter]
			}
			extension := filepath.Ext(output_url)
			if strings.Contains(extension, "css") || strings.Contains(extension, "js") {
				continue
			}
			output_url = core_url.FindAllString(output_url, -1)[0]
			*link_list = append(*link_list, output_url)
		}
	}
}
