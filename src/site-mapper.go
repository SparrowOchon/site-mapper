package main

import (
	"fmt"
	"os"

	"gitlab.com/SparrowOchon/site-mapper/src/lib/thread"
	"gitlab.com/SparrowOchon/site-mapper/src/lib/utility"
)

func main() {
	const URL_INPUT_BUFFER = 1800000
	const THREADPOOL_OUTPUT_BUFFER = 100000
	user_input := validate_input_parameters()

	fmt.Println(*user_input.url)
	fmt.Println(*user_input.output_path)

	url_list := make(chan utility.NewUrls, URL_INPUT_BUFFER)            // Input to the threads in pool
	worker_list := make(chan utility.PageMap, THREADPOOL_OUTPUT_BUFFER) // Output of threads in pool
	write_list := make(utility.ParsedPages)                             // Unique map of urls to be written to a file
	thread_pool, pool_wait_group := thread.Make_Pool(url_list, worker_list)

	// Add User entered Url to Thread-pool List
	first_url := make(utility.NewUrls)
	first_url[*user_input.url] = 0
	url_list <- first_url

	// Read urls fetched from threadpool to see if they duplicates
	threadpool_output_reader(worker_list, url_list, write_list, *user_input.depth, *user_input.url)

	if _, ok := <-worker_list; !ok {
		file_writer(write_list, *user_input.output_path) // Only write to file once we know the threads are all done
	}

	pool_wait_group.Wait()
	thread_pool.Release()
}

/**
 * @Description: Take Threadpool output, compare the URLs in links to those we currently have set to traverse (NewUrls)
	if its new add it to the list to be traversed. If not skip over it.
 * @param worker_list Channel containing the Threadpool output that we will read.
 * @param url_list Channel containing new urls to be scanned by the Threadpool
 * @param write_list Map of collected data to be written to file.
 * @param max_depth User provided max depth to scan to
 * @param parent_url User provided url.
*/
func threadpool_output_reader(worker_list <-chan utility.PageMap, url_list chan<- utility.NewUrls,
	write_list utility.ParsedPages, max_depth int, parent_url string) {
	var is_depth_limit = false    // Track if we currently hit the max depth requested
	var is_urlchan_closed = false // Track if we have finished adding new URLs to read to the threadpool

	/**
	Non-Terminating
	*/
	for {
		fmt.Printf("URLs left to Parse(by Threadpool): %d\n", len(url_list))
		fmt.Printf("Threadpool Data left to analyze: %d\n", len(worker_list))

		// Close the URL channel when, we hit Depth limit and the threadpool is finished scanning
		if is_depth_limit && len(url_list) == 0 && !is_urlchan_closed {
			close(url_list)
			is_urlchan_closed = true
		}
		line, alive := <-worker_list // Read output from threadpool
		if !alive {
			break
		}
		fmt.Printf("Current Depth %d\n", line.NextDepth-1)
		write_list[line.CurrUrl] = line // Add the current page data to be written to file
		if line.NextDepth == max_depth+1 {
			is_depth_limit = true
			continue
		}
		for _, next_url := range line.Pages { // For each additional URL found
			if _, ok := write_list[next_url]; !ok { // Check if the URL is already marked to be written
				if is_same_domain(next_url, parent_url) {
					new_url := make(utility.NewUrls) // If not add the URL to the list to get queried by threadpool
					new_url[next_url] = line.NextDepth
					url_list <- new_url
					write_list[next_url] = utility.PageMap{} // Add a placeholder to avoid duplicate scans
				}
			}
		}
	}
}

/**
 * @Description: Writes a collection of PageMap's to a file in JSON format
 * @param write_list Channel of urls that need to be scanned
 * @param output_path Full file path to write data to
 */
func file_writer(write_list utility.ParsedPages, output_path string) {
	const FILE_PERMISSIONS = 0644

	fmt.Println("Writing to File")

	output_file, err := os.OpenFile(output_path,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, FILE_PERMISSIONS)
	if err != nil {
		fmt.Println("Error: Failed to open Output file: " + output_path)
		os.Exit(1)
	}
	defer output_file.Close()

	for _, val := range write_list {
		if val.CurrUrl == "" {
			continue
		}
		json_bytes, err := utility.JSONMarshal(val)
		if err != nil {
			fmt.Println("Error: Failed to Marshal output JSON")
			break
		}
		if _, err := output_file.WriteString(string(json_bytes)); err != nil {
			fmt.Println("Error: while trying to write to file.")
			break
		}
	}
}

/**
 * @Description: Compare two URLs base URL to see if they are from the same domain
 * @param url1 First url whose base URL we want to check
 * @param url2 Second url whose base URL we want to check
 * @return bool Return True if the sites are the same domain, False if not or if sites are invalid
 */
func is_same_domain(url1 string, url2 string) bool {
	if len(url1) > 0 && len(url2) > 0 {
		return utility.UrlHostname(url1) == utility.UrlHostname(url2)
	}
	return false
}
