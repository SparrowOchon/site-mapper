# Site Mapper
The following program takes a user-defined URL and depth as parameters and creates a JSON output as the site map. The site map will include URLs to pages, links, images, and videos only on the same domain.

## Features:
- Leverages Golang Concurrency via a thread pool using `6 * User CPU Count` threads to parse URLs
- HTML parsers are used over Regex to minimize false positives. (Currently using `a href` and `img src`. `link href` was excluded due to how long runtime would take)
- No Lock based thread communication
- Compatible with Windows, Mac, Linux

## Usage

```
usage: site-mapper [-h|--help] [-m|--max-depth <integer>] [-o|--output
                   "<value>"] -u|--url "<value>"

                   Create a sitemap given a URL.
 Examples:      site-mapper -u http://google.com -m 10


Arguments:

  -h  --help       Print help information
  -m  --max-depth  Max Depth.(Default: 10)
  -o  --output     Output Location (Defaults to PWD/output)
  -u  --url        Url to map
```


## Installation

### Pre-Built
Download and run the pre-built binary for your system from [Releases](https://gitlab.com/SparrowOchon/site-mapper/-/releases)

### From Source
Built using Golang 1.17 using Go Modules
```
go build -o site-mapper ./src #gosetup
```

## How it Works
The following is a really quickly sketched diagram showing the implication of the channels and Threadpool. Many steps are missing from this that are displayed in the code.
![thread-map](images/thread-setup.png)

- Sends URL to thread-pool to scans all Links in first page
- Goes through every Page Link found during by thread-pool and if it's new add it to be scanned as Depth +1
- Keeps scanning I.e repeating step 1 and 2 until depth is reached.
- Write all found URL data to a file.


## Changes Missing/Flaws
- Problem: The Soup library used for Html parsing is severely lacking compared to BeautifulSoup4 and required additional pointer allocation to use accurately increasing runtime
  - Solution: GoQuery is the default library for HTML parsing on Golang, but I took this time to try a new library due to bugs quite heavily underperformed.
  
- Problem: The Depth is only being checked by the Main thread. Even if there is a thread pool that can check 16+ URLs at once. Only one looks through the data parsed to see if it's duplicated and adds it to the list of new URLs to be parsed.
  - Solution: Share a copy of the lookup table directly to the thread pool. Although this will create false sharing and cause cache misses on update, the speed will still be faster than Locking or the current approach.
  
- Problem: The only links filtered out are `CSS` and `js` from the `href` fields. As such, videos will be flagged as a viable `Link` and appear under `links`
  - Solution: Switching to GoQuery would have allowed us to filter by type as well. Which could have aided in filtering out videos without needing Regex.
  
- Problem: Due to the nature of the Main Thread doing the checking and being single-threaded. When Max Depth is too high, the Buffer could get filled and cause a crash
  - Solution: Increasing buffer size is a temp fix. A permanent fix is to thread the nature of the main thread.

- Problem: Sometimes the program freezes for a couple of ms or significantly slows down for a bit and then continues again
  - Solution: This is due to the Mark and Sweep garbage collector. It's in the process of marking and deleting all the heap allocations no longer in use. This can be avoided leveraging Virtual Memory allocations to only allow the program to hold enough data as it needs to allow M&S to run way sooner. Reducing the freeze delays.  